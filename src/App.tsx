/* eslint-disable @typescript-eslint/space-before-function-paren */
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Error404 from './pages/404.tsx'
import Home from './pages/Home.tsx'
import Navbar from './components/ui/layout/Navbar.tsx'
import Footer from './components/ui/layout/Footer.tsx'
import Login from './pages/Login.tsx'
import Registrar from './pages/Registrar.tsx'
import Sucursales from './pages/Sucursales.tsx'
import Products from './pages/Products.tsx'
import Restablecer from './pages/Restablecer.tsx'
import Verificar from './pages/Verificar.tsx'
import { Checkout } from './pages/checkout.tsx'
import Correo from './pages/Correo.tsx'
import ReturnPage from './pages/ReturnPage.tsx'
//import { PRODUCTS_QUERY } from './querys/all_products.ts'
//import { useQuery } from '@apollo/client'

function App(): JSX.Element {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" Component={Home} />
          <Route path="/checkout" Component={Checkout} />
          <Route path="*" Component={Error404} />
          <Route path="/login" Component={Login} />
          <Route path="/login/registrarse" Component={Registrar} />
          <Route path="/verificar" Component={Verificar} />
          <Route path="/sucursales" Component={Sucursales} />
          <Route path="/productos" Component={Products} />
          <Route path="/login/restablecer" Component={Restablecer} />
          <Route path="/correo" Component={Correo} />
          <Route path="/status" Component={ReturnPage} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  )
}

export default App
