import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import { Product } from './Product'
import { afterEach, describe, expect, it } from 'vitest'

const addToCartMock = () => {
  product.quantity++
}
const removeOneFromCart = () => {}

const product = {
  __typename: 'Product',
  id: 2849,
  title: '3D Large Wordmark Pullover Hoodie',
  description:
    'The Unisex 3D Large Wordmark Pullover Hoodie features soft fleece and an adjustable, jersey-lined hood for comfort and coverage. Designed in a unisex style, the pullover hoodie includes a tone-on-tone 3D silicone-printed wordmark across the chest.',
  price: 70,
  type: 'hoodies',
  color: 'black',
  gender: 'unisex',
  imageOne: '8529107-00-A_0_2000.jpg',
  imageTwo: '8529107-00-A_1.jpg',
  branchName: 'Sucursal_6_Poniente',
  branchDirection: '6 Poniente 150',
  quantity: 30,
  size: 'XS',
}

describe('renderizado Product', () => {
  afterEach(cleanup)
  it('render correctly', () => {
    render(
      <Product
        addToCart={addToCartMock}
        product={product}
        removeOneFromCart={removeOneFromCart}
      />
    )
  })

  it('render correctly', () => {
    render(
      <Product
        addToCart={addToCartMock}
        product={product}
        removeOneFromCart={removeOneFromCart}
      />
    )
    const button = screen.getByTestId('add')
    fireEvent.click(button)
    expect(product.quantity === 31)
  })
})
