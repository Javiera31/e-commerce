import { IProductContext } from '../../context/CartContext'
import { IProduct } from '../../interfaces/product'
import { ChangeQuantity } from '../ChangeQuantity'

interface Props {
  product: IProductContext
  addToCart: (product: IProduct) => void
  removeOneFromCart: (product: IProduct) => void
}
export const Product = ({ product, addToCart, removeOneFromCart }: Props) => {
  const AddProduct = () => {
    addToCart(product)
  }
  
  const RemoveOne = () => {
    removeOneFromCart(product)
  }

  return (
    <div
      key={product.id}
      className="bg-[#D9D9D9] flex p-5 gap-7 max-w-[650px] overflow-hidden flex-wrap"
    >
      <ChangeQuantity
        quantity={product.quantity}
        addToCart={AddProduct}
        removeOneFromCart={RemoveOne}
      />

      <img
        className="w-20 h-20 my-4 mb-0 rounded-2xl min-w-[100px]"
        alt={product.title}
        src={'/IProd/' + product.imageOne}
      />
      <h3 className="font-bold text-2xl md:text-3xl max-w-[250px]">
        {product.title}
      </h3>
      <h4 className="font-bold text-xl md:text-2xl">${product.price}</h4>
    </div>
  )
}
