interface Props {
  quantity: number
  addToCart: () => void
  removeOneFromCart: () => void
}
export const ChangeQuantity = ({
  quantity,
  addToCart,
  removeOneFromCart,
}: Props) => {
  return (
    <div className="flex flex-col items-center justify-center">
      <button data-testid="add" onClick={() => addToCart()}>
        <img className="rotate-180" src="/Arrow.svg" />
      </button>

      <h4>{quantity}</h4>

      <button onClick={() => removeOneFromCart()}>
        <img src="/Arrow.svg" />
      </button>
    </div>
  )
}
