import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

//NAVEACIÓN (FLECHAS), PAGINACIÓN (CÍRCULOS) Y AUTOPLAY(SE CAMBIEN SOLAS)
import { Autoplay, Pagination, Navigation } from 'swiper/modules'
import { banner } from '../../assets/banner'

//container w-full mx-auto h-[60vh] max-w-[900px]
const Carousel = (): JSX.Element => {
  return (
    <div className="container w-full mx-auto h-[60vh] max-w-[1700px]">
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper rounded-[32px]"
      >
        {banner.map((product) => (
          <SwiperSlide
            key={product.name}
            className="h-[57vh] mx-auto rounded-[32px]"
          >
            <img className="img" src={product.url} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}
export default Carousel
//h-full w-full object-cover
