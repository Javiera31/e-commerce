import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import { Navigation } from 'swiper/modules'

interface product {
  name: string
  url: string
}

interface props {
  data: product[]
}

export const Carrusel = ({ data }: props): JSX.Element => {
  return (
    <div className="container w-full md:max-w-[1700px] h-[37vh] m-auto">
      <Swiper
        slidesPerView={4}
        spaceBetween={30}
        centeredSlides={false}
        navigation={true}
        modules={[Navigation]}
        className="mySwiper"
      >
        {data.map((product, index) => {
          if (index % 2 === 0) {
            return (
              <SwiperSlide
                key={product.name}
                className="h-[37vh] rounded-[20px]"
              >
                <img
                  className="rounded-[20px] w-full h-full"
                  src={product.url}
                />
              </SwiperSlide>
            )
          }
          return (
            <SwiperSlide
              key={product.name}
              className="h-[37vh] rounded-[20px] max-w-[300px]"
            >
              <img className="rounded-[20px] object-cover" src={product.url} />
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}
