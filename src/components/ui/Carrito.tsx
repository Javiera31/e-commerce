import { useContext } from 'react'
import CartContext from '../../context/CartContext'
import { CartContextType } from '../../context/CartContext'

const Carrito = (): JSX.Element => {
  const { cart } = useContext(CartContext) as CartContextType

  console.log({ cart })

  return (
    <div
      className={
        'fixed p-4 right-0 top-0 bg-blue-100 w-80 h-screen overflow-y-scroll'
      }
    >
      <h1 className="text-white font-bold text-2xl">Your Cart</h1>
      <p className="text-3xl font-bold">Total: </p>
      {cart.map((product) => {
        if (product.quantity !== 0) {
          return (
            <div key={product.id} className="">
              <div className="product-container flex justify-between items-center">
                <div className="flex items-center">
                  <img
                    className="w-20 h-20 my-4 mb-0"
                    alt={product.title}
                    src={product.image}
                  />
                </div>
              </div>
            </div>
          )
        }
      })}
    </div>
  )
}
export default Carrito
