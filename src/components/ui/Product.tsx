// Product.tsx
import { useMutation } from '@apollo/client';
import { ADD_TO_CART_MUTATION } from '../../querys/add_cart'; // Reemplaza con la ubicación real de tus mutaciones
import { IProduct } from '../../interfaces/product';
import { useNavigate } from 'react-router-dom';

interface Props {
  product: IProduct;
}

const Product = ({ product }: Props) => {
  const navigate = useNavigate();
  const [addToCartMutation] = useMutation(ADD_TO_CART_MUTATION);

  const handleAddToCart = async () => {
    const userEmail = localStorage.getItem('userEmail');

    if (userEmail) {
      try {
        const { data } = await addToCartMutation({
          variables: {
            userEmail,
            productId: product.id,
            quantity: 1, // Puedes ajustar la cantidad según tu lógica
          },
        });

        console.log(data.addToCart);

        // Puedes manejar la respuesta aquí, por ejemplo, actualizar el estado del carrito local
      } catch (error) {
        console.error('Error al agregar al carrito:', error);
      }
    } else {
      navigate('/correo');
    }
  };

  return (
    <div className="h-[650px] flex flex-col rounded-lg border-solid border-2 border-gray-700">
      <img src={'/IProd/' + product.imageOne} className="rounded-lg" alt="" />
      <div className="gap-3 text-center p-3 flex flex-col justify-between h-full">
        <p hidden>{product.id}</p>
        <p className="font-bold text-left">{product.title}</p>
        <p className="">{product.description.slice(0, 110)}...</p>
        <p className="">Precio: ${product.price}</p>
        <p className="">Talla: {product.size}</p>
        <p className="">Stock: {product.quantity}</p>
        <button
          onClick={handleAddToCart}
          className="w-full bg-blue-500 py-2 hover:bg-blue-700 text-white font-bold rounded"
        >
          Añadir
        </button>
      </div>
    </div>
  )
};

export default Product;
