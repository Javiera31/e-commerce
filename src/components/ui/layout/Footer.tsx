import { Link } from 'react-router-dom'

const Footer = (): JSX.Element => {
  return (
    <footer className="gap-5 w-full bg-black h-[120px] text-[#ded6d6]">
      <div className="flex items-center gap-5 w-full justify-center">
        <Link to="sucursales" className="font-semibold">
          NUESTRAS SUCURSALES
        </Link>
      </div>
      <div className="mt-4 max-md:hidden border-t-2">
        <span>© TODOS LOS DERECHOS RESERVADOS</span>
        <h2 className="font-semibold">QUATRO ESTACIONES</h2>
      </div>
    </footer>
  )
}
export default Footer
