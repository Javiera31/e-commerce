import React, { createContext, useState, ReactNode, useEffect } from 'react'
import { IProduct } from '../interfaces/product'
import { ADD_TO_CART_MUTATION } from '../querys/add_cart'
import { useMutation } from '@apollo/client'
export interface IProductContext {
  __typename: string
  id: number
  title: string
  description: string
  price: number
  type: string
  color: string
  gender: string
  imageOne: string
  imageTwo: string
  branchName: string
  branchDirection: string
  size: string
  quantity: number
}

export interface CartContextType {
  cart: IProductContext[]
  addToCart: (product: IProduct) => void
  removeFromCart: (productId: number) => void
  removeOneFromCart: (product: IProduct) => void
}

const CartContext = createContext<CartContextType | null>(null)

interface CartProviderProps {
  children?: ReactNode
}

export const CartProvider: React.FC<CartProviderProps> = ({ children }) => {
  const [cart, setCart] = useState<IProductContext[]>(
    JSON.parse(localStorage.getItem('products') ?? '[]')
  )

  const [addToCartMutation] = useMutation(ADD_TO_CART_MUTATION);

  useEffect(() => {
    localStorage.setItem('products', JSON.stringify(cart))
  }, [cart])

  const findProductIndexById = (productId: number) => {
    return cart.findIndex((product) => product.id === productId)
  }

  const addToCart = async (product) => {
    try {
      await addToCartMutation({
        variables: {
          userEmail: localStorage.getItem('userEmail'),
          productId: product.id,
          quantity: 1,
        },
      });
      window.location.reload();
    } catch (error) {
      console.error('Error al agregar al carrito:', error);
    }
  };

  const removeOneFromCart = async (product) => {
    try {
      await addToCartMutation({
        variables: {
          userEmail: 'rodaxx30@outlook.com',
          productId: product.id,
          quantity: -1,
        },
      });
      window.location.reload();
      // Actualizar el estado local si es necesario
      // ...
    } catch (error) {
      console.error('Error al quitar del carrito:', error);
    }
  };

  return (
    <CartContext.Provider
      value={{ cart, addToCart, removeOneFromCart }}
    >
      {children}
    </CartContext.Provider>
  )
}

export default CartContext
