import React, { createContext, useEffect, useState } from 'react'

interface product {
  name: string
  url: string
}

interface props {
  children: React.ReactElement
}

import { products_temporada } from '../assets/nueva_temporada'

export const ProductContext = createContext([])

const ProductProvider = ({ children }: props): JSX.Element => {
  const [products, setProducts] = useState<product[]>([])

  useEffect(() => {
    setProducts(products_temporada)
  }, [])

  return (
    <ProductContext.Provider value={{ products }}>
      {children}
    </ProductContext.Provider>
  )
}

export default ProductProvider
