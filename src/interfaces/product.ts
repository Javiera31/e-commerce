export interface IProduct {
  __typename:      string;
  id:              number;
  title:           string;
  description:     string;
  price:           number;
  type:            string;
  color:           string;
  gender:          string;
  imageOne:        string;
  imageTwo:        string;
  branchName:      string;
  branchDirection: string;
  size:            string;
}


  