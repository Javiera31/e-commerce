/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react'
import ReactDOM from 'react-dom/client'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'

import App from './App.tsx'

import './index.css'
import { CartProvider } from './context/CartContext.tsx'

const client = new ApolloClient({
  uri: import.meta.env.VITE_GRAPHQLURI,
  cache: new InMemoryCache(),
})

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ApolloProvider client={client}>
    <React.StrictMode>
      <CartProvider>
        <App></App>
      </CartProvider>
    </React.StrictMode>
  </ApolloProvider>
)
