const Error = (): JSX.Element => {
  return (
    <>
      <h2 className="font-bold">ERROR: SITIO NO ENCONTRADO</h2>
    </>
  )
}

export default Error
