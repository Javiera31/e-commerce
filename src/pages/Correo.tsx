import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'

const Correo = (): JSX.Element => {
  const navigate = useNavigate()
  const [email, setEmail] = useState('')

  const handleEmailSubmit = () => {
    localStorage.setItem('userEmail', email)
    navigate('/productos')
  }

  return (
    <div className="flex w-full h-screen">
      <div className="w-full flex items-center justify-center">
        <div className="bg-white px-10 py-20 rounded-3xl border-2 border-gray-100">
          <h1 className="text-4xl font-semibold text-black">
            Ingrese su Correo
          </h1>
          <p className="font-medium text-lg text-gray-700 mt-4">
            Ingrese su correo para la boleta electrónica
          </p>
          <div className="mt-7">
            <div>
              <label className="text-lg font-medium text-gray-700">
                Correo Electrónico:
              </label>
              <input
                className="w-full border-2 border-gray-300 rounded-xl p-3 mt-1 bg-transparent"
                placeholder="Ingrese su correo"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <div className="mt-8 flex flex-col gap-y-4">
                <button
                  onClick={handleEmailSubmit}
                  className="active:scale-[0.98] active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-blue-500 text-white text-lg font-bold"
                >
                  Continuar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Correo
