import Carousel from '../components/ui/Carousel'
import { Carrusel } from '../components/ui/CarouselFun'
import { products_vendidos } from '../assets/mas_vendidos'
import { products_temporada } from '../assets/nueva_temporada'
import { Link } from 'react-router-dom'

const Home = (): JSX.Element => {
  return (
    <main className="w-full min-h-screen p-5 pb-16">
      <Link to={'productos'}>Productos</Link>
      <Carousel />
      <h2 className="text-[30px] ml-3">Más vendidos </h2>
      <Carrusel data={products_vendidos} />
      <h2 className="text-[30px] pt-16 ml-3">Nueva temporada</h2>
      <Carrusel data={products_temporada} />
    </main>
  )
}
export default Home
