import { FormEvent, useEffect, useRef, useState } from 'react'
import { Link, redirect, useLocation, useNavigate } from 'react-router-dom'

const Login = (): JSX.Element => {
  const loc = useLocation()

  useEffect(() => {
    console.log(loc)
  }, [loc])

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = (ev: FormEvent<HTMLFormElement>) => {
    ev.preventDefault()
    console.log(password)
  }

  return (
    <div className="flex w-full  h-screen">
      <div className=" w-full flex items-center justify-center ">
        <form
          onSubmit={handleSubmit}
          className=" bg-white px-10 py-20  rounded-3xl border-2 border-gray-100"
        >
          <h1 className="text-4xl font-semibold">Iniciar Sesión</h1>
          <div className="mt-7">
            <div>
              <label className="text-lg font-medium">Correo Electronico </label>
              <input
                onChange={({ target }) => setEmail(target.value)}
                className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
                placeholder=" Ingrese su correo"
                type="email"
              />
            </div>
            <div>
              <label className="text-lg font-medium"> Contraseña </label>
              <input
                onChange={({ target }) => setPassword(target.value)}
                className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
                placeholder=" Ingrese su contraseña"
                type="password"
              />
            </div>

            <div className="mt-8 flex justify-between items-center">
              <div className="font-medium text-base text-blue-500 ">
                <Link
                  to="restablecer"
                  className="font-medium text-base text-blue-500"
                >
                  Restablecer contraseña
                </Link>
              </div>
            </div>

            <div className=" mt-8 flex flex-col gap-y-4">
              <button className="active:scale-[0.98]  active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-black text-white text-lg font-bold">
                {' '}
                Ingresar
              </button>
            </div>

            <div className="mt-8 flex justify-center items-center">
              ¿No tienes cuenta?
              <Link
                to="registrarse"
                className="font-medium text-base text-blue-500"
              >
                Registrarse
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}
export default Login
