//import { useQuery } from '@apollo/client'
import { useQuery } from '@apollo/client'
import { PRODUCTS_QUERY } from '../querys/all_products'
import Product from '../components/ui/Product'
import { IProduct } from '../interfaces/product'
//import { data } from '../try_jsons/products.json'

// const prods = data.products

const Products = (): JSX.Element => {
  const { loading, error, data } = useQuery(PRODUCTS_QUERY)
  const prods = data && data.products ? data.products.slice(0, 100) : []
  console.log({ prods })
  //if (loading) return <p>Loading...</p>
  //if (error) return <p>Error: {error.message}</p>

  return (
    <div className="m-10 flex justify-center">
      <div className="sm:grid sm:grid-cols-3 flex flex-wrap gap-5 max-w-[870px] container">
        {prods.map((product: IProduct) => (
          <Product key={product.id} product={product} />
        ))}
      </div>
    </div>
  )
}
export default Products
