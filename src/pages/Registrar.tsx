import { useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

const Registrar = (): JSX.Element => {
  const loc = useLocation()
  useEffect(() => {
    console.log(loc)
  }, [loc])

  const navigate = useNavigate()

  return (
    <div className=" w-full flex items-center  justify-center bg-gray-200">
      <div className="px-10 py-20 bg-white rounded-3xl border-2 border-gray-200 my-5">
        <div className="mt-3">
          <h1 className="text-5xl font-semibold">Registrarte</h1>
          <p className="font-medium text-lg text-gray-500 mt-4">
            {' '}
            Ingrese sus datos personales y disfruta de una experiencia de
            compras divertidas.
          </p>
        </div>
        <div className=" mt-8">
          <label className="text-lg font-medium"> Nombre </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder=" Ingrese su nombre"
          />
        </div>

        <div className="flex flex-col mt-4">
          <label className="text-lg font-medium"> Apellido </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder="Ingrese su apellido aqui "
          />
        </div>

        <div>
          <label className="text-lg font-medium"> Rut </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder="Ingrese su rut aqui "
            type="rut"
          />
        </div>
        <div>
          <label className="text-lg font-medium"> Celular </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder="Ingrese su numero"
          />
        </div>
        <div>
          <label className="text-lg font-medium">Correo Electronico </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder=" Ingrese su correo"
          />
        </div>
        <div>
          <label className="text-lg font-medium"> Contraseña </label>
          <input
            className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
            placeholder="Ingrese su contraseña"
            type="password"
          />
        </div>

        <div className=" mt-6 flex flex-col gap-y-3">
          <button
            onClick={() => navigate('/')}
            className="active:scale-[0.98] active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-black text-white text-lg font-bold"
          >
            {' '}
            Registrarse
          </button>
        </div>
      </div>
    </div>
  )
}

export default Registrar
