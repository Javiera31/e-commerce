import { Link } from 'react-router-dom'
const Restablecer = (): JSX.Element => {
  return (
    <div className=" flex w-full  h-screen">
      <div className=" w-full flex items-center justify-center ">
        <div className=" bg-white px-10 py-20  rounded-3xl border-2 border-gray-100">
          <h1 className="text-4xl font-semibold">Restablecer contraseña</h1>
          <p className="font-medium text-lg text-gray-500 mt-4">
            {' '}
            Ingrese su correo para cambiar su contraseña
          </p>
          <div className="mt-7">
            <div>
              <label className="text-lg font-medium">
                Correo Electronico :{' '}
              </label>
              <input
                className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
                placeholder=" Ingrese su correo"
                type="email"
              />
              <div className=" mt-8 flex flex-col gap-y-4">
                <button className="active:scale-[0.98]  active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-black text-white text-lg font-bold">
                  <Link
                    to="/verificar"
                    className="font-medium text-base border-y-white"
                  >
                    continuar
                  </Link>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Restablecer
