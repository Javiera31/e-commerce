import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useMutation } from '@apollo/client';
import { CONFIRM_TRANSACTION_MUTATION } from '../querys/confirmTransaction';

const ReturnPage = () => {
  const location = useLocation();
  const [transactionInfo, setTransactionInfo] = useState<{
    vci: any;
    amount: any;
    status: any;
    buy_order: any;
  } | undefined>(undefined);
  const [confirmTransaction] = useMutation(CONFIRM_TRANSACTION_MUTATION);

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const token_ws = searchParams.get('token_ws');

    if (token_ws) {
      confirmTransaction({
        variables: {
          token: token_ws,
        },
      })
        .then(result => {
          const { vci, amount, status, buy_order } = result.data.confirmTransaction;
          const transactionDetails = {
            vci: vci,
            amount: amount,
            status: status,
            buy_order: buy_order,
          };
          setTransactionInfo(transactionDetails);
        })
        .catch(error => {
          console.error('Error al confirmar la transacción:', error);
          setTransactionInfo(undefined);
        });
    } else {
      setTransactionInfo(undefined);
    }
  }, [location.search, confirmTransaction]);

  return (
    <div className="flex flex-col items-center justify-center h-screen">
      <h1 className="text-3xl font-bold mb-4">Estado de la Transacción</h1>
      {transactionInfo ? (
        <div className='flex flex-col items-center '>
          <p>VCI: {transactionInfo.vci}</p>
          <p>Monto: {transactionInfo.amount}</p>
          <p>Estado: {transactionInfo.status}</p>
          <p>Orden de compra: {transactionInfo.buy_order}</p>
        </div>
      ) : (
        <p>No hay información de transacción disponible.</p>
      )}
    </div>
  );
};

export default ReturnPage;
