import { sucursales } from '../assets/sucursales'

const Sucursales = (): JSX.Element => {
  return (
    <div className="p-4 flex flex-wrap justify-center items-center gap-4">
      {sucursales.map((sucursal) => (
        <div key={sucursal.nombre} className="flex flex-col items-center">
          <img
            src={sucursal.url}
            className="h-[57vh] mx-auto my-5"
            alt={sucursal.nombre}
          />
          <h2 className="font-bold">{sucursal.nombre}</h2>
        </div>
      ))}
    </div>
  )
}

export default Sucursales
//container w-full mx-auto max-w-[1700px] max-h-68
//box-border h-70 w-50
