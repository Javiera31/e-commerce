import { Link } from 'react-router-dom'

const Verificar = (): JSX.Element => {
  return (
    <div className=" flex w-full  h-screen">
      <div className=" w-full flex items-center justify-center ">
        <div className=" bg-white px-10 py-20  rounded-3xl border-2 border-gray-100">
          <h1 className="text-4xl font-semibold">Restablecer contraseña</h1>
          <p className="font-medium text-lg text-gray-500 mt-4">
            {' '}
            Ingrese una nueva contraseña para su cuenta y evita que se repita
            con las que utilizaste anteriormente
          </p>
          <div className="mt-7">
            <div>
              <label className="text-lg font-medium">Ingrese el codigo </label>
              <input
                className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
                placeholder=" Ingrese el codigo"
                type="code"
              />
            </div>
            <div>
              <label className="text-lg font-medium"> Nueva contraseña </label>
              <input
                className="w-full border-2 border-gray-100 rounded-xl p-3 mt-1 bg-transparent"
                placeholder="Ingrese una contraseña"
                type="password"
              />
            </div>
            <div className=" mt-6 flex flex-col gap-y-3">
              <button className="active:scale-[0.98] active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-black text-white text-lg font-bold">
                <Link
                  to="/login"
                  className="font-medium text-base border-white"
                >
                  Crear
                </Link>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Verificar
