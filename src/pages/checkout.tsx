import { useContext, useEffect, useState } from "react";
import { useMutation, useQuery } from "@apollo/client";
import { Link } from "react-router-dom";
import { BUY_MUTATION } from "../querys/buy_cart";
import { CART_QUERY } from "../querys/get_cart"; // Reemplaza con la ubicación real de tu archivo cartQuery
import CartContext, { CartContextType } from "../context/CartContext";
import { Product } from "../components/Cart/Product";

export const Checkout = (): JSX.Element => {
  const [buyCart, { loading: buyLoading, error: buyError, data: buyData }] = useMutation(BUY_MUTATION);
  const { cart, addToCart, removeOneFromCart } = useContext(CartContext) as CartContextType;

  const { loading: cartLoading, error: cartError, data: cartData } = useQuery(CART_QUERY, {
    variables: {
      userEmail: localStorage.getItem("userEmail") || "",
    },
  });

  const [initialValue, setInitialValue] = useState(0);

  useEffect(() => {
    const init = 0;
    if (cartData && cartData.cart) {
      setInitialValue(cartData.cart.total);
    }
  }, [cartData]);

  const handlePurchase = async () => {
    try {
      const result = await buyCart({
        variables: {
          email: localStorage.getItem("userEmail"),
        },
      });
  
      if (result.data && result.data.payOrder) {
        const { url, error } = result.data.payOrder;
  
        if (url) {
          window.location.href = url;
        } else if (error) {
          alert(error);
  
  
        }
      }
    } catch (error) {
      console.error("Error during purchase:", error);

    }
  };
  
  

  if (buyLoading || cartLoading) {
    return <p>Cargando...</p>;
  }

  if (buyError || cartError) {
    console.error("Error during mutation/query:", buyError || cartError);
    return <p>Error al cargar datos.</p>;
  }


  return (
    <div className="min-h-screen md:p-10">
      <h1 className="font-bold bg-[#EDEDED] rounded-3xl p-5 inline-block text-2xl mb-5">
        Carrito de compras
      </h1>
      <div className="flex justify-center gap-5 flex-wrap">
        <div className="flex flex-col justify-between gap-10">
          {cartData &&
            cartData.cart &&
            cartData.cart.products.map((product) => {
              if (product.quantity !== 0) {
                return (
                  <Product
                    key={product.id}
                    product={product}
                    addToCart={addToCart}
                    removeOneFromCart={removeOneFromCart}
                  />
                );
              }
            })}
        </div>

        <section className="min-w-[300px] bg-[#D9D9D9] rounded-3xl p-5 flex flex-col gap-10">
          <h2 className="text-3xl font-semibold">Resumen</h2>
          <h3 className="font-semibold border-b-2 border-[#000] text-xl">
            Total <span className="font-normal">${initialValue}</span>
          </h3>
          <button
            onClick={handlePurchase}
            className="bg-[#A6A4A4] px-3 py-2 text-[#000] w-full font-bold text-2xl"
          >
            Continuar
          </button>
          <Link className="w-full text-center" to="/productos">
            Seguir comprando
          </Link>
        </section>
      </div>
    </div>
  );
};

export default Checkout;
