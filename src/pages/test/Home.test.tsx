import { cleanup, render } from '@testing-library/react'
import Home from '../Home'
import { CartProvider } from '../../context/CartContext'
import { BrowserRouter } from 'react-router-dom'
import { afterEach, describe, it } from 'vitest'

describe('renderizado Home', () => {
  afterEach(cleanup)
  it('render correctly', () => {
    render(
      <CartProvider>
        <BrowserRouter>
          <Home />
        </BrowserRouter>
      </CartProvider>
    )
  })
})
