import { cleanup, render } from '@testing-library/react'
import { Checkout } from '../checkout'
import { CartProvider } from '../../context/CartContext'
import { BrowserRouter } from 'react-router-dom'
import { afterEach, describe, it } from 'vitest'

describe('renderizado 404', () => {
  afterEach(cleanup)
  it('render correctly', () => {
    render(
      <CartProvider>
        <BrowserRouter>
          <Checkout />
        </BrowserRouter>
      </CartProvider>
    )
  })
})
