import gql from 'graphql-tag'

export const ADD_TO_CART_MUTATION = gql`
  mutation AddToCart($userEmail: String!, $productId: Int!, $quantity: Int!) {
    addToCart(addToCart: {
      userEmail: $userEmail,
      productId: $productId,
      quantity: $quantity,
    }) {
      userEmail
      status
      products {
        id
        quantity
      }
    }
  }
`;