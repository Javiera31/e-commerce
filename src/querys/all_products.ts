import gql from 'graphql-tag'

export const PRODUCTS_QUERY = gql`
  {
    products {
      id
      title
      description
      price
      type
      color
      gender
      imageOne
      imageTwo
      branchName
      branchDirection
      quantity
      size
    }
  }
`;
