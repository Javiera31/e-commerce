import { gql } from "@apollo/client";

export const BUY_MUTATION = gql`
  mutation PayOrder($email: String!) {
    payOrder(email: $email) {
      url
      error
    }
  }
`;
