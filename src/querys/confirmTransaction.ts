import { gql } from '@apollo/client';

export const CONFIRM_TRANSACTION_MUTATION = gql`
  mutation ConfirmTransaction($token: String!) {
    confirmTransaction(token: $token) {
      vci
      amount
      status
      buy_order
    }
  }
`;
