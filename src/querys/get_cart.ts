import { gql } from "@apollo/client";

export const CART_QUERY = gql`
  query GetCart($userEmail: String!) {
    cart(userEmail: $userEmail) {
      userEmail
      status
      total
      products {
        id
        quantity
        imageOne
        title
      }
    }
  }
`;
