// import { configDefaults, defineConfig } from 'vitest/config'
import { defineConfig } from 'vitest/config'
import react from '@vitejs/plugin-react-swc'

export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: 'jsdom',
    // exclude: [
    //   ...configDefaults.exclude, 
    //   './src/assets/**',
    // ],
    // setupFiles: ['./src/test/setup.ts'],
  },
})
